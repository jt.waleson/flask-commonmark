from flask import Blueprint, render_template_string
from flask_commonmark import Commonmark

blueprint = Blueprint("commonmark", __name__)
commonmark = Commonmark()

@blueprint.route("/test_inline")
def view_render_inline():
    mycm = "Hello, *commonmark*."
    return render_template_string("{{mycm|commonmark}}", mycm=mycm)

@blueprint.route("/test_var_block")
def view_render_var_block():
    mycm = u"Hello, *commonmark* block."
    template = """\
{% filter commonmark %}
{{mycm}}
{% endfilter %}"""
    return render_template_string(template, mycm=mycm)

@blueprint.route("/test_in_block")
def view_render_in_block():
    template = u"""\
{% filter commonmark %}
Hello, *commonmark* block.
{% endfilter %}"""
    return render_template_string(template)

@blueprint.route("/test_autoescape_on")
def view_render_autoescape_on():
    mycm = "Hello, <b>commonmark</b> block."
    return render_template_string(
        ("{% autoescape true %}"
         "{{mycm|commonmark}}"
         "{% endautoescape %}"), mycm=mycm)

@blueprint.route("/test_autoescape_off")
def view_render_autoescape_off():
    mycm = "Hello, <b>commonmark</b> block."
    result = render_template_string(
        ("{% autoescape false %}"
         "{{mycm|commonmark}}"
         "{% endautoescape %}"), mycm=mycm)
    return result

